import models.BigDog;
import models.Cat;
import models.Dog;

public class App {
    public static void main(String[] args) throws Exception {
       Cat cat = new Cat("meo");
       Dog dog = new Dog("cho");
       BigDog bigDog = new BigDog("cholon");

       cat.greet();
       dog.greet();
       bigDog.greet();
    }
}
